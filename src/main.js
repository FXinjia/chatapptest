import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import firebase from 'firebase'

// Required for side-effects
require("firebase/firestore");

  var firebaseConfig = {
    apiKey: "AIzaSyARSH_ttV6XZbE3Z3NxPxHAI87EeyDEh-s",
    authDomain: "chatapplication-f5a21.firebaseapp.com",
    databaseURL: "https://chatapplication-f5a21.firebaseio.com",
    projectId: "chatapplication-f5a21",
    storageBucket: "chatapplication-f5a21.appspot.com",
    messagingSenderId: "667319162415",
    appId: "1:667319162415:web:00db0db76f6cf8db6f18da",
    measurementId: "G-25VHV33ZMQ"
  };
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

  var db = firebase.firestore();
  window.db= db;

  Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
